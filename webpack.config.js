var webpack = require('webpack')
module.exports = {
    entry:__dirname + '/index.js',
    output: {
        path: __dirname + '/dist',
        filename:'index.js'
    },
    module:{
        rules:[{
            test:/\.js$/,
            use:{
                loader:'babel-loader',
            },
            exclude:/node_modules/
        },{
            test:/\.od$/,
            use:{
                loader:'odloader',
            },
            exclude:/node_modules/
        }]
    },
    externals: {
        'Designer':'window.okdesigner',
    }
}